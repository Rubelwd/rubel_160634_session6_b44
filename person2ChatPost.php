<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Person2 Chat Window</title>
    <style>
        .container,.result {
            width: 250px;
            height: 50px;
            margin: 5px auto;
            background: #ddd;
            text-align: center;
            padding: 50px;
            border-radius: 500px;
            border:1px solid #FFCE43;
            font-family: "Monotype Corsiva";
            color: #DC5E26;
        }
        input[type="submit"] {
            border: 1px solid #fff;
            color: #fff;
            paddind: 7px;
            background: #449D44;
        }
    </style>
</head>
<body>



<div class="container">
    <table>
        <form action="person1ChatGet.php" method="post">
            <label for="username">Person2:</label>
            <input type="text" name="chatMsg"><br><br>
            <input type="submit">
        </form>
    </table>
</div>

<div class="result">
    <?php

    if(isset($_POST['chatMsg'])){
        echo "Person1 Message<br>".$_POST['chatMsg'];
    }


    ?>
</div>




</body>
</html>